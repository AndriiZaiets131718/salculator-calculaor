﻿using AppiumLocal.Tests.POM;
using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using TechTalk.SpecFlow;

namespace AppiumLocal.Tests.Steps
{
    [Binding]
    public class FunctionalTestingSteps
    {
        private AndroidDriver<AndroidElement> _driver;
        public MainScreen mainScreen;

        [BeforeScenario]
        public void CreateDriver()
        {
            AppiumOptions caps = new AppiumOptions();

            caps.AddAdditionalCapability("app", @"D:\\DevEducation\\CalculatorRep\\salculator-calculaor\\apkarchive\\com.companyname.calculaor.apk");
            caps.AddAdditionalCapability("deviceName", "Google Pixel 2");
            caps.AddAdditionalCapability("automationName", "UIAutomator2");
            caps.AddAdditionalCapability("platformVersion", "9.0");
            caps.AddAdditionalCapability("platformName", "Android");
            caps.AddAdditionalCapability("build", "CSharp Android_Sim");
            caps.AddAdditionalCapability("name", "second_test");

            _driver = new AndroidDriver<AndroidElement>(
                    new Uri("http://localhost:4723/wd/hub"), caps);
            mainScreen = new MainScreen(_driver);
        }

        [Given(@"Tapped on number ""(.*)""")]
        public void GivenTappedOnNumber(string p0)
        {
            mainScreen.TapOnNumberButton(p0);
        }
               
        [When(@"Tap on delete button")]
        public void WhenTapOnDeleteButton()
        {
            mainScreen.TapOnDeleteButton();
        }
        
        [When(@"Tap on divide button")]
        public void WhenTapOnDivideButton()
        {
            mainScreen.TapOnOperationButton("÷");
        }
        
        [When(@"Tap on number (.*)")]
        public void WhenTapOnNumber(string p0)
        {
            mainScreen.TapOnNumberButton(p0);
        }
        
        [When(@"Tap on equal button")]
        public void WhenTapOnEqualButton()
        {
            mainScreen.TapOnEqualButton();
        }
        
        [When(@"Tap on plus button")]
        public void WhenTapOnPlusButton()
        {
            mainScreen.TapOnOperationButton("+");
        }
        
        [When(@"Tap on point button")]
        public void WhenTapOnPointButton()
        {
            mainScreen.TapOnPointButton();
        }
        
        [When(@"Tap on multiply button")]
        public void WhenTapOnMultiplyButton()
        {
            mainScreen.TapOnOperationButton("*");
        }
        
        [When(@"Tap on minus button")]
        public void WhenTapOnMinusButton()
        {
            mainScreen.TapOnOperationButton("-");
        }
        
        [When(@"Tap on operation button (.*)")]
        public void WhenTapOnOperationButton(string p0)
        {
            mainScreen.TapOnOperationButton(p0);
        }
        
        [Then(@"The result field displays the number (.*)")]
        public void ThenTheResultFieldDisplaysTheNumber(string p0)
        {
            Assert.AreEqual(p0, mainScreen.GetTextFromResultField());
        }

        [AfterScenario]
        public void KillDriver()
        {
            _driver.Quit();
        }
    }
}
