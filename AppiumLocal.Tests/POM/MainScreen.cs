﻿using OpenQA.Selenium;
using OpenQA.Selenium.Appium.Android;

namespace AppiumLocal.Tests.POM
{
    public class MainScreen
    {
        private AndroidDriver<AndroidElement> _driver;

        public MainScreen(AndroidDriver<AndroidElement> driver)
        {
            _driver = driver;
        }

        public By oneButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[11]");
        public By twoButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[12]");
        public By threeButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[13]");
        public By fourButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[7]");
        public By fiveButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[8]");
        public By sixButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[9]");
        public By sevenButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[3]");
        public By eightButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[4]");
        public By nineButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[5]");
        public By zeroButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[16]");
        public By deleteButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[1]");
        public By pointButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[15]");
        public By divideButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[2]");
        public By multiplyButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[6]");
        public By plusButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[14]");
        public By minusButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[10]");
        public By equalButton = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.view.ViewGroup/android.widget.Button[17]");
        public By resultField = By.XPath("/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.HorizontalScrollView/android.widget.TextView");

        public MainScreen TapOnNumberButton(string number)
        {
            switch (number)
            {
                case "1":
                    _driver.FindElement(oneButton).Click();
                    break;
                case "2":
                    _driver.FindElement(twoButton).Click();
                    break;
                case "3":
                    _driver.FindElement(threeButton).Click();
                    break;
                case "4":
                    _driver.FindElement(fourButton).Click();
                    break;
                case "5":
                    _driver.FindElement(fiveButton).Click();
                    break;
                case "6":
                    _driver.FindElement(sixButton).Click();
                    break;
                case "7":
                    _driver.FindElement(sevenButton).Click();
                    break;
                case "8":
                    _driver.FindElement(eightButton).Click();
                    break;
                case "9":
                    _driver.FindElement(nineButton).Click();
                    break;
                case "0":
                    _driver.FindElement(zeroButton).Click();
                    break;
            }
            return this;
        }

        public MainScreen TapOnOperationButton(string operation)
        {
            switch (operation)
            {
                case "+":
                    _driver.FindElement(plusButton).Click();
                    break;
                case "-":
                    _driver.FindElement(minusButton).Click();
                    break;
                case "÷":
                    _driver.FindElement(divideButton).Click();
                    break;
                case "*":
                    _driver.FindElement(multiplyButton).Click();
                    break;
            }
            return this;
        }

        public MainScreen TapOnPointButton()
        {
            _driver.FindElement(pointButton).Click();
            return this;
        }

        public MainScreen TapOnDeleteButton()
        {
            _driver.FindElement(deleteButton).Click();
            return this;
        }

        public MainScreen TapOnEqualButton()
        {
            _driver.FindElement(equalButton).Click();
            return this;
        }

        public AndroidElement FindElementWithTextFromResultField()
        {
            return _driver.FindElement(resultField);
        }

        public string GetTextFromResultField()
        {
            return FindElementWithTextFromResultField().Text;
        }
    }
}
