﻿using Appium.Tests.POM;
using NUnit.Framework;
using OpenQA.Selenium.Appium;
using OpenQA.Selenium.Appium.Android;
using System;
using TechTalk.SpecFlow;

namespace Appium.Tests.Steps
{
    [Binding]
    public class FunctionalTestingSteps
    {
        public AndroidDriver<AndroidElement> driver;
        public MainScreen mainScreen;

        [BeforeScenario]
        public void CreateDriver()
        {
            AppiumOptions caps = new AppiumOptions();

            // Set your BrowserStack access credentials
            caps.AddAdditionalCapability("browserstack.user", "learnqa_gkOjhL");
            caps.AddAdditionalCapability("browserstack.key", "wK7H6QQzgAhHXemKzzv6");

            // Set URL of the application under test
            caps.AddAdditionalCapability("app", "bs://aadfbfa4275fd0c8ba3066241e716dabf64d91ae");

            // Specify device and os_version
            caps.AddAdditionalCapability("device", "Google Pixel 2");
            caps.AddAdditionalCapability("os_version", "9.0");

            // Specify the platform name
            caps.PlatformName = "Android";

            // Set other BrowserStack capabilities
            caps.AddAdditionalCapability("project", "First CSharp project");
            caps.AddAdditionalCapability("build", "CSharp Android");
            caps.AddAdditionalCapability("name", "first_test");

            // Initialize the remote Webdriver using BrowserStack remote URL
            // and desired capabilities defined above
            driver = new AndroidDriver<AndroidElement>(
                    new Uri("http://hub-cloud.browserstack.com/wd/hub"), caps);
            mainScreen = new MainScreen(driver);
        }

        [Given(@"Tapped on number (.*)")]
        public void GivenTappedOnNumber(string p0)
        {
            mainScreen.TapOnNumberButton(p0);
        }

        [Given(@"Tapped on point button")]
        public void GivenTappedOnPointButton()
        {
            mainScreen.TapOnPointButton();
        }

        [When(@"Tap on point button")]
        public void WhenTapOnPointButton()
        {
            mainScreen.TapOnPointButton();
        }

        [When(@"Tap on delete button")]
        public void WhenTapOnDeleteButton()
        {
            mainScreen.TapOnDeleteButton();
        }
        
        [When(@"Tap on divide button")]
        public void WhenTapOnDivideButton()
        {
            mainScreen.TapOnDivideButton();
        }
        
        [When(@"Tap on equal button")]
        public void WhenTapOnEqualButton()
        {
            mainScreen.TapOnEqualButton();
        }
        
        [When(@"Tap on plus button")]
        public void WhenTapOnPlusButton()
        {
            mainScreen.TapOnPlusButton();
        }
        
        [When(@"Tap on multiply button")]
        public void WhenTapOnMultiplyButton()
        {
            mainScreen.TapOnMultiplyButton();
        }

        [When(@"Tap on minus button")]
        public void WhenTapOnMinusButton()
        {
            mainScreen.TapOnMinusButton();
        }

        [When(@"Tap on number (.*)")]
        public void WhenTapOnNumber(string p0)
        {
            mainScreen.TapOnNumberButton(p0);
        }

        [Then(@"The result field displays the number (.*)")]
        public void ThenTheResultFieldDisplaysTheNumber(string p0)
        {
           Assert.AreEqual(p0, mainScreen.GetTextFromResultField());
        }

        [AfterScenario]
        public void KillDriver()
        {
            driver.Quit();
        }
    }
}
