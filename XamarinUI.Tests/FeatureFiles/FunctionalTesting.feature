﻿Feature: Functional testing
	As a user
	I want to be able to add two numbers
	In order to get sum of two numbers

	As a user
	I want to be able to divide two numbers
	In order to know the quotient

	As a user
	I want to be able to subtract two numbers
	In order to see the difference between them

	As a user
	I want to be able to multiply two numbers
	In order to see their product

	As I user
	I want to get answer after math operation
	So that I can get answer without press button equal "+"

	As I user
	I want to change math operation after choose math operation
	So that I shouldn't delete numbers which I entered

	As a user
	I want to be able to reset the calculator
	In order to start over

Scenario: Delete number
	Given Tapped on number 7
	When Tap on delete button
	Then The result field displays the number 0

Scenario Outline: Division two numbers
	Given Tapped on number <first_number>
	When Tap on divide button
	And Tap on number <second_number>
	And Tap on equal button
	Then The result field displays the number <result>

	Examples:
		| first_number | second_number | result     |
		| 4            | 2             | 2          |
		| 5            | 0             | Infinity   |
		| 0            | 3             | 0          |

Scenario Outline: Addition two numbers
	Given Tapped on number <first_number>
	When Tap on plus button
	And Tap on number <second_number>
	And Tap on plus button
	Then The result field displays the number <result>

	Examples:
		| first_number | second_number | result |
		| 1            | 8             | 9      |
		| 0            | 9             | 9      |

Scenario Outline: Multiply two float numbers
	Given Tapped on number <first_number_before_point>
	When Tap on point button
	And Tap on number <first_number_after_point>
	And Tap on multiply button
	And Tap on number <second_number_before_point>
	And Tap on point button
	And Tap on number <second_number_after_point>
	And Tap on equal button
	Then The result field displays the number <result>

	Examples:
		| first_number_before_point | first_number_after_point | second_number_before_point | second_number_after_point | result |
		| 0                         | 2                        | 5                          | 7                         | 1.14   |
		| 3                         | 7                        | 7                          | 3                         | 27.01  |
		| 5                         | 9                        | 2                          | 1                         | 12.39  |
		| 8                         | 5                        | 9                          | 4                         | 79.9   |
		| 9                         | 1                        | 3                          | 9                         | 35.49  |

Scenario Outline: Subtraction two numbers
	Given Tapped on number <first_number>
	When Tap on minus button
	And Tap on number <second_number>
	And Tap on minus button
	Then The result field displays the number <result>

	Examples:
		| first_number | second_number | result |
		| 3            | 2             | 1      |
		| 1            | 9             | -8     |
		| 8            | 9             | -1     |
		| 5            | 3             | 2      |

Scenario Outline: Get answer without press button equal
	Given Tapped on number <first_number_before_point>
	When Tap on point button
	And Tap on number <first_number_after_point>
	And Tap on operation button <math_operation_button>
	And Tap on number <second_number_before_point>
	And Tap on point button
	And Tap on number <second_number_after_point>
	And Tap on plus button
	Then The result field displays the number <result>

Examples:
		| first_number_before_point | first_number_after_point | math_operation_button | second_number_before_point | second_number_after_point | result |
		| 8                         | 7                        | +                     | 2                          | 5                         | 11.2   |
		| 2                         | 3                        | -                     | 1                          | 0                         | 1.3    |
		| 9                         | 0                        | *                     | 5                          | 3                         | 47.7   |
		| 4                         | 6                        | ÷                     | 2                          | 0                         | 2.3    |

Scenario: Changing math operation 
	Given Tapped on number 6
	When Tap on plus button
	And Tap on divide button
	And Tap on number 2 
	And Tap on equal button
	Then The result field displays the number 3