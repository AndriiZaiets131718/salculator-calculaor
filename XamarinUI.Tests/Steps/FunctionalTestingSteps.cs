﻿using NUnit.Framework;
using System;
using TechTalk.SpecFlow;
using UI.Tests.POM;

namespace UI.Tests.Steps
{
    [Binding]
    public class FunctionalTestingSteps
    {
        MainScreen _mainScreen = new MainScreen();

        [BeforeScenario]
        public void BeforeEachTest()
        {
            AppInitializer.StartApp();
        }

        [Given(@"Tapped on number (.*)")]
        public void GivenTappedOnNumber(string p0)
        {
            _mainScreen.TapOnNumberButton(p0);
        }

        [When(@"Tap on operation button (.*)")]
        public void WhenTapOnOperationButton(string p0)
        {
            _mainScreen.TapOnActionButton(p0);
        }

        [When(@"Tap on delete button")]
        public void WhenTapOnDeleteButton()
        {
            _mainScreen.TapOnDelete();
        }
        
        [When(@"Tap on divide button")]
        public void WhenTapOnDivideButton()
        {
            _mainScreen.TapOnActionButton("÷");
        }
        
        [When(@"Tap on equal button")]
        public void WhenTapOnEqualButton()
        {
            _mainScreen.TapOnEqual();
        }
        
        [When(@"Tap on plus button")]
        public void WhenTapOnPlusButton()
        {
            _mainScreen.TapOnActionButton("+");
        }

        [When(@"Tap on point button")]
        public void WhenTapOnPointButton()
        {
            _mainScreen.TapOnPoint();
        }
        
        [When(@"Tap on number (.*)")]
        public void WhenTapOnNumber(string p0)
        {
            _mainScreen.TapOnNumberButton(p0);
        }
        
        [When(@"Tap on multiply button")]
        public void WhenTapOnMultiplyButton()
        {
            _mainScreen.TapOnActionButton("*");
        }
        
        [When(@"Tap on minus button")]
        public void WhenTapOnMinusButton()
        {
            _mainScreen.TapOnActionButton("-");
        }
        
        [Then(@"The result field displays the number (.*)")]
        public void ThenTheResultFieldDisplaysTheNumber(string p0)
        {
            Assert.AreEqual(p0, _mainScreen.GetTextFromField());
        }
    }
}
