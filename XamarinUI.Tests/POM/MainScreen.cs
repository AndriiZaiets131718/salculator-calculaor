﻿using Xamarin.UITest;
using Query = System.Func<Xamarin.UITest.Queries.AppQuery, Xamarin.UITest.Queries.AppQuery>;

namespace UI.Tests.POM
{
    public class MainScreen
    {
        public static IApp App => AppInitializer.App;

        public static void Repl()
        {
            App.Repl();
        }

        public Query textField = x => x.Id("input");
        public Query deleteButton = x => x.Marked("DEL");
        public Query pointButton = x => x.Marked(".");
        public Query equalButton = x => x.Marked("=");

        public string GetTextFromField()
        {
            return App.Query(textField)[0].Text.Trim();
        }

        public MainScreen TapOnNumberButton(string number)
        {
            App.Tap(x => x.Marked(number));
            return this;
        }

        public MainScreen TapOnActionButton(string action)
        {
            App.Tap(x => x.Marked(action));
            return this;
        }

        public MainScreen TapOnPoint()
        {
            App.Tap(pointButton);
            return this;
        }

        public MainScreen TapOnEqual()
        {
            App.Tap(equalButton);
            return this;
        }

        public MainScreen TapOnDelete()
        {
            App.Tap(deleteButton);
            return this;
        }
    }
}
