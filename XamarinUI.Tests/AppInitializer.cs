﻿using Xamarin.UITest;

namespace UI.Tests
{
    public static class AppInitializer
    {
        static IApp _app;
        private static string _simID = null;
        private static string _apkPath = @"D:\\DevEducation\\CalculatorRep\\salculator-calculaor\\Calculator.Xamarin\\bin\\Release\\com.companyname.calculaor.apk";

        /// <summary>
        /// Конструкция, которая будет возвращать нам связь с запущенным приложением.
        /// </summary>
        public static IApp App
        {
            get { return _app; }
        }

        /// <summary>
        /// Будет запускать приложение на эмуляторе.
        /// </summary>
        public static void StartApp()
        {
            var configuratorAndroid = ConfigureApp
                                        .Android
                                        .EnableLocalScreenshots();

            if (_simID != null)
            {
                configuratorAndroid.DeviceSerial(_simID);
            }
            _app = configuratorAndroid
                    .ApkFile(_apkPath)
                    .StartApp();
        }
    }
}
