﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Android.Views;
using calculator;

namespace CalculatorXamarin
{
    [Activity(Label = "@string/app_name", Theme = "@android:style/Theme.DeviceDefault.NoActionBar", MainLauncher = true)]
    public class MainActivity : Activity
    {
        private TextView _calcText;
        private string[] _numbers = new string[2];
        private string @operator;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);
            _calcText = FindViewById<TextView>(Resource.Id.input);
            _numbers[0] = "0";
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
        [Java.Interop.Export("ButtonClick")]
        public void ButtonClick(View v)
        {
            Button button = (Button)v;
            if ("0123456789.".Contains(button.Text))
                AddButtonValues(button.Text);
            else if ("÷*+-".Contains(button.Text))
                AddOperator(button.Text);
            else if ("=" == button.Text)
                Calculate();
            else
                Clear();
        }
        private void AddButtonValues(string value)
        {
            int index = @operator == null ? 0 : 1;
            if (value == "." && _numbers[index].Contains("."))
                return;
            _numbers[index] += value;
            UpdateCalculatorText();
        }
        private void AddOperator(string value)
        {
            if(_numbers[1] != null)
            {
                Calculate();
                return;
            }
            @operator = value;
            UpdateCalculatorText();
        }
        private void Calculate(string newOperator = null)
        {
            double? result = null;
            double? first = _numbers[0] == null ? null : (double?)double.Parse(_numbers[0]);
            double? second = _numbers[1] == null ? null : (double?)double.Parse(_numbers[1]);

                switch (@operator)
            {
                case "÷":
                    result = first / second;
                    break;
                case "*":
                    result = first * second;
                    break;
                case "+":
                    result = first + second;
                    break;
                case "-":
                    result = first - second;
                    break;
            }
            if(result != null)
            {
                _numbers[0] = result.ToString();
                @operator = newOperator;
                _numbers[1] = null;
                UpdateCalculatorText();
            }
            
        }
        private void Clear()
        {
            _numbers[0] = "0";
            _numbers[1] = null;
            @operator = null;
            UpdateCalculatorText();
        }
        private void UpdateCalculatorText()
        {
            _calcText.Text = $"{_numbers[0]}{@operator}{_numbers[1]}";
        }
    }
}